import {DocumentNumberService} from './DocumentNumberService';
import DocumentNumberSeriesItem from './models/DocumentNumberSeriesItem';
import path from 'path';
import {TraceUtils} from '@themost/common';
/**
 * @class
 * The default application service for managing document numbers and document series
 */
class DefaultDocumentNumberService extends DocumentNumberService {
    constructor(app) {
        super(app);
    }

    async next() {
        //
    }

    /**
     * Adds the specified file in a document series
     * @param {DataContext} context - The current context
     * @param {string} file - The full path of a file
     * @param {DocumentNumberSeriesItem} attributes A set of attributes that is going to be attached in document
     */
    async add(context, file, attributes) {
        return await new Promise((resolve, reject) => {
            let finalResult;
            context.db.executeInTransaction( callback => {
                if (attributes.contentType == null) {
                    return reject(new TypeError('Content type cannot be empty at this context.'))
                }
                /**
                 * @type PrivateContentService
                 */
                const svc = context.getApplication().getService( function PrivateContentService() {
                });
                // throw error if PrivateContentService is null
                if (svc == null) {
                    return callback('Content service may not be empty at this context.');
                }
                // copy item
                const newItem = Object.assign({
                    additionalType: 'DocumentNumberSeriesItem'
                }, attributes);
                return context.unattended((cb)=> {
                    // move file to content
                    return svc.copyFrom(context,  path.resolve(process.cwd(), file), newItem, err => {
                            if (err) {
                                TraceUtils.error(err);
                                return cb(new Error('An internal server error occured while moving content.'));
                            }
                            return cb();
                        });
                }, err => {
                    if (err) {
                        return callback(err);
                    }
                    // get model
                    const model = context.model(DocumentNumberSeriesItem);
                    // and customize it in order to disable base model (Attachment)
                    // do migrate
                    return model.migrate( err => {
                        if (err) {
                            return callback(err);
                        }
                        // load primary key
                        model.getPrimaryKey();
                        // override and destroy base model
                        model.base = function() {
                            return null;
                        }
                        // forcibly insert item
                        return model.insert(newItem, (err)=> {
                            if (err) {
                                return callback(err);
                            }
                            return svc.resolveUrl(context, newItem, function(err, url) {
                                if (err) {
                                    return callback(err);
                                }
                                // format result
                                finalResult = Object.assign({
                                    }, newItem, {
                                    url: url
                                });
                                return callback();
                            });
                        });
                    });
                });

            }, (err)=> {
                if (err) {
                    return reject(err);
                }
                return resolve(finalResult);
            });
        });
    }

    /**
     * Replaces the specified file in a document series
     * @param {DataContext|*} context - The current context
     * @param {string} file - The full path of a file
     * @param {DocumentNumberSeriesItem} attributes A set of attributes that is going to be attached in document
     */
    async replace(context, file, attributes) {
        return await new Promise((resolve, reject) => {
            let finalResult;
            context.db.executeInTransaction( callback => {
                if (attributes.contentType == null) {
                    return reject(new TypeError('Content type cannot be empty at this context.'))
                }
                /**
                 * @type PrivateContentService
                 */
                const svc = context.getApplication().getService( function PrivateContentService() {
                });
                // throw error if PrivateContentService is null
                if (svc == null) {
                    return callback('Content service may not be empty at this context.');
                }
                // copy item
                const saveItem = Object.assign({
                    additionalType: 'DocumentNumberSeriesItem'
                }, attributes);
                return context.unattended((cb)=> {
                    // move file to content
                    return svc.copyFrom(context,  path.resolve(process.cwd(), file), saveItem, err => {
                        if (err) {
                            TraceUtils.error(err);
                            return cb(new Error('An internal server error occured while moving content.'));
                        }
                        return cb();
                    });
                }, err => {
                    if (err) {
                        return callback(err);
                    }
                    // get model
                    const model = context.model(DocumentNumberSeriesItem);
                    // and customize it in order to disable base model (Attachment)
                    // do migrate
                    return model.migrate( err => {
                        if (err) {
                            return callback(err);
                        }
                        // load primary key
                        model.getPrimaryKey();
                        // override and destroy base model
                        model.base = function() {
                            return null;
                        }
                        // save item
                        return model.save(saveItem).then(() => {
                            return svc.resolveUrl(context, saveItem, function(err, url) {
                                if (err) {
                                    return callback(err);
                                }
                                // format result
                                finalResult = Object.assign({
                                }, saveItem, {
                                    url: url
                                });
                                // and return
                                return callback();
                            });
                        }).catch( (err) => {
                            return callback(err);
                        });
                    });
                });

            }, (err)=> {
                if (err) {
                    return reject(err);
                }
                return resolve(finalResult);
            });
        });
    }
}

export {
    DefaultDocumentNumberService
}