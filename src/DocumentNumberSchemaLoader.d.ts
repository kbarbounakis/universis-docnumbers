import {FileSchemaLoaderStrategy} from '@themost/data';
import { ConfigurationBase } from '@themost/common';
export declare class DocumentNumberSchemaLoader extends FileSchemaLoaderStrategy {
    constructor(config: ConfigurationBase);
}