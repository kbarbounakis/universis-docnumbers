import {EdmMapping} from "@themost/data";
import DocumentNumberSeries from './DocumentNumberSeries';

@EdmMapping.entityType('DepartmentDocumentNumberSeries')
class DepartmentDocumentNumberSeries extends DocumentNumberSeries {
    constructor() {
        super();
    }
    /**
     *
     * @param {*} index
     * @param {DocumentNumberFormatOptions=} formatOptions
     * @return {string}
     */
    async format(index, formatOptions) {
        // get department current year
        const currentYear = await this.getModel()
            .where('id').equal(this.id)
            .select('department/currentYear as currentYear')
            .silent()
            .value();
        const finalFormatOptions = Object.assign({
            currentDate: new Date(),
            academicYear: currentYear
        }, formatOptions)
        return super.format(index, finalFormatOptions);
    }
}
module.exports = DepartmentDocumentNumberSeries;