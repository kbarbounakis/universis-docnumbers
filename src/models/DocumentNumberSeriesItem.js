/// <reference path="./DocumentNumberSeriesItem.d.ts" />
import {DataObject, EdmMapping, EdmType} from '@themost/data';
import {readStream} from "@themost/express";
import { Guid } from '@themost/common';

@EdmMapping.entityType('DocumentNumberSeriesItem')
class DocumentNumberSeriesItem extends DataObject {
    //
    async newCode() {
        return Guid.newGuid().toString().replace(/-/g,'');
    }

    /**
     * @param {Buffer|ArrayBuffer|ReadableStream} file
     * @param {*} attributes
     * @returns Promise<DocumentNumberSeriesItem>
     */
    @EdmMapping.param('attributes', 'Object', true, true)
    @EdmMapping.param('file', EdmType.EdmStream, false)
    @EdmMapping.action('replace','DocumentNumberSeriesItem')
    async replace(file, attributes) {
        let blob;
        // get file buffer
        if (file instanceof Buffer) {
            blob = file;
        } else if (file instanceof ArrayBuffer) {
            blob = new Uint8Array(file);
        } else {
            blob = await readStream(file);
        }
        // get extra attributes and assign id
        const extraAttributes = Object.assign({
            contentType: file.contentType,
            name: file.contentFileName
        }, attributes, {
            id: this.getId()
        });
        // get document service by name
        const documentService = this.context.getApplication().getService(function DocumentNumberService(){
        });
        // throw exception if document service is null
        if (documentService == null) {
            throw new Error('Document service cannot be found or is inaccessible.');
        }
        // replace file
        await documentService.replaceFrom(this.context, blob, extraAttributes);
        // return extraAttributes object
        return extraAttributes;
    }
}

module.exports = DocumentNumberSeriesItem;