/// <reference path="./DocumentNumberSeries.d.ts" />
import {Args, DataError} from '@themost/common';
import {DataObject, EdmMapping} from '@themost/data';
import numeral from 'numeral';
import moment from 'moment';

@EdmMapping.entityType('DocumentNumberSeries')
class DocumentNumberSeries extends DataObject {
    async next() {
        // validate id
        Args.notNumber(this.id, 'Item identifier');
        // get id
        const id = this.id;
        // get number format if empty
        const model = this.context.model(DocumentNumberSeries);
        let lastIndex;
        await new Promise((resolve, reject) => {
            this.context.db.executeInTransaction( (cb) => {
                // execute async method
               (async function() {
                   // get lastIndex
                   lastIndex = await model.where('id').equal(id).select('lastIndex').silent().value();
                   lastIndex += 1;
                   // update lastIndex
                   await model.silent().save({
                       id,
                       lastIndex
                   });
               })().then( () => {
                    return cb();
               }).catch( err => {
                    return cb(err);
               });
            }, (err) => {
                if (err) {
                    return reject(err);
                }
                return resolve();
            });
        });
      return lastIndex;
    }
    /**
     * 
     * @param {*} index 
     * @param {DocumentNumberFormatOptions=} formatOptions 
     * @return {string}
     */
    async format(index, formatOptions) {
        // get format params
        const options = Object.assign({ 
            currentDate: new Date()
         }, formatOptions);
         // fetch numberFormat if empty
        if (this.numberFormat == null) {
            this.numberFormat = await this.getModel().select('numberFormat').silent().value();
        }
        Args.notNull(this.numberFormat, 'Document number format');
        let result = this.numberFormat;
        // first find number parameter e.g. {0000} or {0}
        let match = /\{(\d+)\}/.exec(result);
        if (match) {
            // e.g. match[0] => {0000} and match[1] => 0
            const formattedIndex = numeral(index).format(match[1]);
            // replace formatted index
            result = result.replace(/\{(\d+)\}/, formattedIndex);
        }
        else {
            // throw error for invalid expression
            throw new Error('Invalid document number format expression. Index parameter is missing');
        }
        if (formatOptions.academicYear) {
            const academicYear = this.context.model('AcademicYear').convert(formatOptions.academicYear).getId();
            if (Number.isInteger(academicYear) === false) {
                throw new DataError('E_VALUE', 'Invalid academic year. Expected number.', null, 'AcademicYear', 'id');
            }
            if (/{AAAAAA}/g.test(result)) {
                // replace {AAAAAA} with academicYear long format e.g 2019-2020
                result = result.replace(/{AAAAA}/i, `${academicYear}-${academicYear+1}`);
            }
            if (/{AAAAA}/g.test(result)) {
                // replace {AAAAA} with academicYear alternate name e.g 2019-20
                result = result.replace(/{AAAAA}/i, `${academicYear}-${(academicYear+1).toString().substr(2)}`);
            }
            if (/{AAAA}/g.test(result)) {
                // replace {AAAA} with academic year e.g 2019
                result = result.replace(/{AAAA}/i, academicYear);
            }
        }

        // format date parameters if any
        const re = /\{(.*?)\}/i;
        match = re.exec(result);
        let momentInstance = moment(options.currentDate);
        while(match) {
            // e.g. match[0] => {YYYY} and match[1] => YYYY
            let replaceMatch = momentInstance.format(match[1]);
            result = result.replace(match[0], replaceMatch);
            match = re.exec(result);
        }
        return result;
    }
    /**
     * Gets next index and format document number
     * @param {DocumentNumberFormatOptions=} formatOptions 
     * @return {string}
     */
    async nextAndFormat(formatOptions) {
        const nextIndex = await this.next();
        return this.format(nextIndex, formatOptions);
    }
}
module.exports = DocumentNumberSeries;